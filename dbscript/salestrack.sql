-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: salestrack
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `AGENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `AGENT_FIRST_NAME` varchar(45) DEFAULT NULL,
  `AGENT_LAST_NAME` varchar(45) DEFAULT NULL,
  `AGENT_EMAIL` varchar(99) DEFAULT NULL,
  `AGENT_PHONE` varchar(255) DEFAULT NULL,
  `MESSAGE` longtext,
  `ADDRESS` longtext,
  `LAST_UPDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATE_DATE` varchar(45) DEFAULT NULL,
  `COMPANY_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`AGENT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `COMPANY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_NAME` varchar(45) DEFAULT NULL,
  `COMPANY_REGISTER_DATE` datetime DEFAULT NULL,
  `COMPANY_TYPE` varchar(45) DEFAULT NULL,
  `COMPANY_LOGO` varchar(45) DEFAULT NULL,
  `IS_ACTIVE` varchar(3) DEFAULT NULL,
  `COMPANY_LOGIN_NAME` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'SalesTrack','2018-11-11 00:17:29','Marketing','salestracklogo.png','yes','salestrack');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dealer`
--

DROP TABLE IF EXISTS `dealer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dealer` (
  `DEALER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AGENT_ID` int(11) NOT NULL,
  `DEALER_FIRST_NAME` varchar(45) DEFAULT NULL,
  `DEALER_LAST_NAME` varchar(45) DEFAULT NULL,
  `DEALER_EMAIL` varchar(99) DEFAULT NULL,
  `DEALER_PHONE` varchar(255) DEFAULT NULL,
  `MESSAGE` longtext,
  `REGISTER_DATE` longtext,
  `LATITUDE` varchar(45) DEFAULT NULL,
  `LONGITUDE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DEALER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_ID` int(11) NOT NULL,
  `USER_FIRST_NAME` varchar(45) DEFAULT NULL,
  `USER_LAST_NAME` varchar(45) DEFAULT NULL,
  `USER_CREATE_DATE` varchar(45) DEFAULT NULL,
  `USER_ROLE` varchar(45) DEFAULT NULL,
  `USER_TYPE` varchar(45) DEFAULT NULL,
  `USER_PROFILE_PIC` varchar(45) DEFAULT NULL,
  `USER_EMAIL` varchar(99) DEFAULT NULL,
  `USER_PHONE` varchar(255) DEFAULT NULL,
  `ADDRESS` longtext,
  `PASSWORD` varchar(99) DEFAULT NULL,
  `USERNAME` varchar(99) DEFAULT NULL,
  `IS_ACTIVE` varchar(99) DEFAULT NULL,
  `DEACTIVATION_DATE` varchar(99) DEFAULT NULL,
  `AGENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'Administrator','User','2018-11-10 23:35:57',NULL,'1','avatar.png',NULL,NULL,NULL,'t0n1ght','admin','yes',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-14  1:01:23
