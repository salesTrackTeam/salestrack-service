package com.salestrack.app.utils;

public interface Constants {
	static final String GET_USER_BY_ID = "/getUser/{userId}";
	static final String GET_ALL_USERS = "/getAllUsers";
	static final String SAVE_USER = "/saveUser";
	static final String GET_DEALER_BY_ID = "/getDealer/{userId}";
	static final String GET_ALL_DEALER = "/getAllDealer";
	static final String SAVE_DEALER = "/saveDealer";
	static final String GET_DEALER_BY_AGENT_ID = "/getDealerByAgentId/{agentId}";
	static final String GET_AGENT_BY_ADMIN_ID = "getAgentByAdminId/{userId}/{companyId}";
	static final String GET_AGENT_PROFILEDATA = "getAgentProfileData/{userId}/{companyId}/{agentId}";
	static final String ADD_AGENT = "/addAgent";
	static final String MODIFY_AGENT = "/modifyAgent";
	static final String GET_ADMIN_DASHBOARD_DATA = "/getAdminDashboardData/{userId}/{companyId}";
	static final String AUTHENTICATE = "/authenticate/{company}/{username}/{password}";
	static final String GET_DEALER_BY_ADMIN_ID = "getDealerByAdminId/{userId}/{companyId}/{agentId}";
	static final String ENQUIRY = "/enquiry";
	static final String GET_DEALER_DETAILS = "/getDealerDetails/{userId}/{companyId}/{dealerId}/{fromWhere}";
	static final String SIGNUP = "/signup";
}
