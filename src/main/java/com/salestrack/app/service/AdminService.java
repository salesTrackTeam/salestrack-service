package com.salestrack.app.service;

import java.util.List;

import com.salestrack.app.dto.AgentDto;
import com.salestrack.app.dto.AdminDashboardDto;
import com.salestrack.app.dto.DealerDto;

/**
 * Created by Prashant on 13/5/17.
 */
public interface AdminService {
    //UserDto getUserById(Integer userId);
    String addAgent(AgentDto agentDto);
    List<AgentDto> getAgentByUserId(Integer userId,Integer companyId);
    AdminDashboardDto getAdminDashboardData(Integer userId,Integer companyId);
    List<DealerDto> getDealerByAdminId(Integer userId,Integer companyId,Integer agentId);
    AgentDto getAgentProfileData(Integer userId,Integer companyId,Integer agentId);
    DealerDto getDealerDetails(Integer userId, Integer companyId, Integer dealerId, String fromWhere);
    String modifyAgent(AgentDto agentDto);
}
