package com.salestrack.app.service;

import java.util.List;

import com.salestrack.app.dto.UserDto;

/**
 * Created by Prashant on 13/5/17.
 */
public interface UserService {
    UserDto authenticate(String company, String userName, String password);
    String signup(UserDto userDto);
}
