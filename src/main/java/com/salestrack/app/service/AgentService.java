package com.salestrack.app.service;

import java.util.List;

import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.UserDto;

/**
 * Created by Prashant on 13/5/17.
 */
public interface AgentService {
    //UserDto getUserById(Integer userId);
    String AddDealer(DealerDto dealerDto);
    List<DealerDto> getDealerByAgentId(Integer agentId);
}
