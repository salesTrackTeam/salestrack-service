package com.salestrack.app.service;

import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.EnquiryDto;

public interface HomeService {
	String SubmitEnquiry(EnquiryDto enquiryDto);
}
