package com.salestrack.app.service.impl;

import com.salestrack.app.connection.DatabaseConnection;
import com.salestrack.app.converter.UserConverter;
import com.salestrack.app.dto.AgentDto;
import com.salestrack.app.dto.UserDto;
import com.salestrack.app.repository.UserRepository;
import com.salestrack.app.service.UserService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Prashant on 13/5/17.
 */
@Service
public class UserServiceimpl implements UserService {

	@Override
	public UserDto authenticate(String companyLoginName,String userName,String password) {
		String userId ="";
		String companyId = "";
		String companyName = "";
		String userFirstName ="";
		String userLastName = "";
		String userRole = "";
		String userType = "";
		String profilePic = "";
		String email = "";
		String phone = "";
		String address = "";
		String isActive = "";
		String deactivationDate = "";
		String loginFlag = "";
		String agentId = "";

		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AgentDto> agentList = new ArrayList();
		String query = "SELECT U.USER_ID,U.COMPANY_ID,U.USER_FIRST_NAME,U.USER_LAST_NAME,U.USER_CREATE_DATE," +
				"U.USER_ROLE,U.USER_TYPE,U.USER_PROFILE_PIC,U.USER_EMAIL,U.USER_PHONE,U.ADDRESS,U.PASSWORD," +
				"U.USERNAME,U.IS_ACTIVE,U.DEACTIVATION_DATE,C.COMPANY_ID,C.COMPANY_NAME,C.COMPANY_REGISTER_DATE," +
				"C.COMPANY_TYPE, C.COMPANY_LOGIN_NAME, C.COMPANY_LOGO,C.IS_ACTIVE,U.AGENT_ID FROM user U, company C " +
				"WHERE U.COMPANY_ID = C.COMPANY_ID AND U.USERNAME = ? AND C.COMPANY_LOGIN_NAME = ?";
		try{
			ps = DatabaseConnection.getDatabaseNewConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, userName);
			ps.setString(2, companyLoginName);
			rs = ps.executeQuery();
			
			System.out.println("query:"+query);
			System.out.println("userName:"+userName);
			System.out.println("companyLoginName:"+companyLoginName);
			if(rs.next()) {
				userId = rs.getString("U.USER_ID");
				companyId = rs.getString("U.COMPANY_ID");
				companyLoginName = rs.getString("C.COMPANY_LOGIN_NAME");
				companyName = rs.getString("C.COMPANY_NAME");
			    userFirstName = rs.getString("U.USER_FIRST_NAME");
			    userLastName = rs.getString("U.USER_LAST_NAME");
			    userRole = rs.getString("U.USER_ROLE");
			    userType = rs.getString("U.USER_TYPE");
			    profilePic = rs.getString("U.USER_PROFILE_PIC");
			    email = rs.getString("U.USER_EMAIL");
			    phone = rs.getString("U.USER_PHONE");
			    address = rs.getString("U.ADDRESS");
			    isActive = rs.getString("U.IS_ACTIVE");
				deactivationDate = rs.getString("USER_CREATE_DATE");	
				agentId = rs.getString("U.AGENT_ID");
				
				//conditions-------------------------start
				if(!password.equals(rs.getString("U.PASSWORD"))){
					loginFlag = "2";
				}else if(rs.getString("U.IS_ACTIVE")!=null && "no".equalsIgnoreCase(rs.getString("U.IS_ACTIVE")))
				{
					loginFlag = "3";
				}else if(rs.getString("C.IS_ACTIVE")!=null && "no".equalsIgnoreCase(rs.getString("C.IS_ACTIVE"))){
					loginFlag = "4";
				}
				else{
					loginFlag = "1";
				}
				//conditions-------------------------end
				}
			else{
				loginFlag = "5";//User or Company does Not exist.
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		UserDto userData = new UserDto(userId,companyId,companyLoginName,companyName,userFirstName,userLastName,userRole,userType,profilePic,email,phone,address,isActive,deactivationDate,loginFlag,agentId);
		return userData;
	}
	
	 /*
	 * @author: Mukesh Singla
	 * @Purpose: This method used to define addCustomer method used to add customer in the system during Sign Up process.
*/
@Override
public String signup(UserDto userDto) {
	try{
		//validation//
		PreparedStatement ps = DatabaseConnection.getDatabaseNewConnection().prepareStatement("SELECT * FROM company WHERE COMPANY_LOGIN_NAME='"+userDto.getCompanyLoginName()+"'", Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = ps.executeQuery("SELECT * FROM company WHERE COMPANY_LOGIN_NAME='"+userDto.getCompanyLoginName()+"'");
		if(rs.next()){
			return "{\"result\":\"companyLoginNameExists\"}";
		}
		String query = "INSERT INTO company(COMPANY_NAME,COMPANY_TYPE,COMPANY_LOGO,IS_ACTIVE,COMPANY_LOGIN_NAME, COMPANY_REGISTER_DATE,REGISTRATION_TYPE) values(?,?,?,?,?,NOW(),?)";

		ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, userDto.getCompanyName().trim());
		ps.setString(2, userDto.getCompanyType().trim());
		ps.setString(3, "");
		ps.setString(4, "yes");
		ps.setString(5, userDto.getCompanyLoginName().trim());
		ps.setString(6, userDto.getRegistrationType().trim());
		ps.execute();
		rs = ps.getGeneratedKeys();
		if(rs.next()) {
			java.math.BigDecimal companyId = rs.getBigDecimal(1);     

			String query2 = "INSERT INTO user(USERNAME,PASSWORD,COMPANY_ID,USER_FIRST_NAME,USER_LAST_NAME,USER_CREATE_DATE,USER_PROFILE_PIC,USER_TYPE,IS_ACTIVE,USER_PHONE,USER_EMAIL)" +
					" values(?,?,?,?,?,NOW(),?,?,?,?,?)";
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, userDto.getUserName().trim());
			ps.setString(2, userDto.getPassword().trim());
			ps.setInt(3, companyId.intValue());
			ps.setString(4, userDto.getUserFirstName());
			ps.setString(5, userDto.getUserLastName());
			ps.setString(6, "avatar.png");
			ps.setString(7, "1");
			ps.setString(8, "yes");
			ps.setString(9, userDto.getPhone());
			ps.setString(10, userDto.getEmail());
			ps.execute();
		}
		ps.close();
	}catch(Exception e){
		e.printStackTrace();
		return "{\"result\":\"failed\"}";
	}
	return "{\"result\":\"success\"}";
}
}
