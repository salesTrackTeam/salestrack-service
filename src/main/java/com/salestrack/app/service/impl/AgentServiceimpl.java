package com.salestrack.app.service.impl;

import com.salestrack.app.connection.DatabaseConnection;
import com.salestrack.app.converter.UserConverter;
import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.UserDto;
import com.salestrack.app.repository.UserRepository;
import com.salestrack.app.service.AgentService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
/**
 * Created by Prashant on 13/5/17.
 */
@Service
public class AgentServiceimpl implements AgentService {

	@Override
	public String AddDealer(DealerDto dealerDto) {
		String query = "INSERT INTO dealer(AGENT_ID,DEALER_FIRST_NAME,DEALER_LAST_NAME,DEALER_EMAIL,DEALER_PHONE,MESSAGE,REGISTER_DATE,LATITUDE,LONGITUDE) values(?,?,?,?,?,?,NOW(),?,?)";
		try{
			PreparedStatement ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, dealerDto.getAgentId());
			ps.setString(2, dealerDto.getFirstName());
			ps.setString(3, dealerDto.getLastName());
			ps.setString(4, dealerDto.getEmail());
			ps.setString(5, dealerDto.getPhone());
			ps.setString(6, dealerDto.getMessage());
			ps.setString(7, dealerDto.getLatitude());
			ps.setString(8, dealerDto.getLongitude());
			ps.execute();
			ps.close();
			return "{\"result\":\"success\"}";
		}catch(Exception e){
			e.printStackTrace();
			return "{\"result\":\"failed\"}";
		}
	}

	@Override
	public List<DealerDto> getDealerByAgentId(Integer agentId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<DealerDto> dealerList = new ArrayList();
		String query = "SELECT AGENT_ID,DEALER_ID,DEALER_FIRST_NAME,DEALER_LAST_NAME,DEALER_EMAIL,DEALER_PHONE,MESSAGE,REGISTER_DATE,LATITUDE,LONGITUDE FROM dealer WHERE AGENT_ID = ?";
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, agentId);
			rs = ps.executeQuery();
			while (rs.next()) {
				agentId = rs.getInt("AGENT_ID");
				Integer dealerId = rs.getInt("DEALER_ID");
				String firstName = rs.getString("DEALER_FIRST_NAME");	
				String lastName = rs.getString("DEALER_LAST_NAME");	
				String email = rs.getString("DEALER_EMAIL");	
				String phone = rs.getString("DEALER_PHONE");	
				String message = rs.getString("MESSAGE");	
				String registerDate = rs.getString("REGISTER_DATE");	
				String latitude = rs.getString("LATITUDE");	
				String longitude = rs.getString("LONGITUDE");	
				dealerList.add(new DealerDto(agentId, dealerId, firstName, lastName, email, phone, message,registerDate,latitude,longitude));
				System.out.println("111 "+agentId+ dealerId+ firstName+ lastName+ email+ phone+ message+registerDate+latitude+longitude);
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("222 "+dealerList);
		return dealerList;
	}
}
