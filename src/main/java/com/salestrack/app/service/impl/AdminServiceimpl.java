package com.salestrack.app.service.impl;

import com.salestrack.app.connection.DatabaseConnection;
import com.salestrack.app.converter.UserConverter;
import com.salestrack.app.dto.AdminDashboardDto;
import com.salestrack.app.dto.AgentDto;
import com.salestrack.app.dto.CompositeTransaction;
import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.RecentAgentActivity;
import com.salestrack.app.dto.ToDoList;
import com.salestrack.app.dto.UserDto;
import com.salestrack.app.dto.RecentTrade;
import com.salestrack.app.repository.UserRepository;
import com.salestrack.app.service.AdminService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.lang.Math;

import org.springframework.stereotype.Service;
/**
 * Created by Prashant on 13/5/17.
 */
@Service
public class AdminServiceimpl implements AdminService {

	@Override
	public AdminDashboardDto getAdminDashboardData(Integer userId,Integer companyId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		AdminDashboardDto adminDashboardData = new AdminDashboardDto();
		List<RecentTrade> recentTrade = new ArrayList();
		List<RecentAgentActivity> recentAgentActivity = new ArrayList();
		List<CompositeTransaction> compositeTransaction = new ArrayList();
		List<ToDoList> toDoList = new ArrayList();
		String query = "SELECT IF(D.AGENT_ID IS NULL, 0 , COUNT(*)) AS NO_DEALER, A.AGENT_ID,A.USER_ID,A.AGENT_FIRST_NAME,A.AGENT_LAST_NAME,A.AGENT_EMAIL,A.AGENT_PHONE,";
		query+= " A.MESSAGE,A.ADDRESS,A.CREATE_DATE FROM agent A LEFT JOIN dealer D ON A.AGENT_ID = D.AGENT_ID ";
		query+= " WHERE USER_ID = ? AND COMPANY_ID = ? GROUP BY A.AGENT_ID  ORDER BY CREATE_DATE DESC LIMIT 6" ;
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, userId);
			ps.setInt(2, companyId);
			rs = ps.executeQuery();
			while (rs.next()) {
				String createDate = rs.getString("CREATE_DATE");
				String userFirstName = rs.getString("AGENT_FIRST_NAME");
				String userLastName = rs.getString("AGENT_LAST_NAME");
				String noOfDealer = rs.getString("NO_DEALER");
				recentAgentActivity.add(new RecentAgentActivity("pending",createDate,userFirstName+" "+userLastName,"0.00",noOfDealer));
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
			query = "SELECT D.AGENT_ID,D.DEALER_ID,D.DEALER_FIRST_NAME,D.DEALER_LAST_NAME,A.AGENT_FIRST_NAME, A.AGENT_LAST_NAME ,D.DEALER_EMAIL,D.DEALER_PHONE,D.MESSAGE,D.REGISTER_DATE,D.LATITUDE,D.LONGITUDE ";
			query += " FROM dealer D LEFT JOIN agent A on A.AGENT_ID = D.AGENT_ID where A.USER_ID = ? and A.COMPANY_ID = ? ORDER BY REGISTER_DATE DESC LIMIT 15";	
			try{
				ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setInt(2, companyId);
				rs = ps.executeQuery();
				while (rs.next()) {
					String agentFirstName = rs.getString("AGENT_FIRST_NAME");
					String agentLastName = rs.getString("AGENT_LAST_NAME");
					Integer dealerId = rs.getInt("DEALER_ID");
					String firstName = rs.getString("DEALER_FIRST_NAME");	
					String lastName = rs.getString("DEALER_LAST_NAME");	
					String email = rs.getString("DEALER_EMAIL");	
					String phone = rs.getString("DEALER_PHONE");	
					String message = rs.getString("MESSAGE");	
					String registerDate = rs.getString("REGISTER_DATE");	
					String latitude = rs.getString("LATITUDE");	
					String longitude = rs.getString("LONGITUDE");
					recentTrade.add(new RecentTrade(agentFirstName+" "+agentLastName, registerDate, "0.00", firstName+" "+lastName));
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			adminDashboardData.setMonthlyIncome("0.00");
			adminDashboardData.setOrders("0");
			adminDashboardData.setVisits(""+recentTrade.size());
			adminDashboardData.setUserActivity("0");

			adminDashboardData.setRecentTrade(recentTrade);
			

			adminDashboardData.setRecentAgentActivity(recentAgentActivity);
			
			compositeTransaction.add(new CompositeTransaction("n/a","n/a","n/a"));
			adminDashboardData.setCompositeTransaction(compositeTransaction);
			
			toDoList.add(new ToDoList("ToDoList1"));

			adminDashboardData.setToDoList(toDoList);
			
			

		return adminDashboardData;
	}
	
	@Override
	public String addAgent(AgentDto agentDto) {
		ResultSet rs;
		String agentUsername = "";
		String query = "INSERT INTO agent(USER_ID,AGENT_FIRST_NAME,AGENT_LAST_NAME,AGENT_EMAIL,AGENT_PHONE,MESSAGE,ADDRESS,CREATE_DATE,COMPANY_ID) values(?,?,?,?,?,?,?,NOW(),?)";
		try{
			PreparedStatement ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, agentDto.getUserId());
			ps.setString(2, agentDto.getAgentFirstName());
			ps.setString(3, agentDto.getAgentLastName());
			ps.setString(4, agentDto.getAgentEmail());
			ps.setString(5, agentDto.getAgentPhone());
			ps.setString(6, agentDto.getMessage());
			ps.setString(7, agentDto.getAddress());
			ps.setInt(8, agentDto.getCompanyId());
			ps.execute();
			rs = ps.getGeneratedKeys();
			if(rs.next()) {
				  java.math.BigDecimal agentId = rs.getBigDecimal(1);     
				  while(true){
					  agentUsername = agentDto.getAgentFirstName().trim()+Integer.toString((int)Math.floor(Math.random()*100000+1));
					  rs = ps.executeQuery("SELECT USERNAME FROM user WHERE USERNAME='"+agentUsername+"' AND COMPANY_ID = '"+agentDto.getCompanyId()+"'");
					  if(rs.next()){
						  continue;
					  }else{
						  String query2 = "INSERT INTO user(USERNAME,PASSWORD,AGENT_ID,COMPANY_ID,USER_FIRST_NAME,USER_LAST_NAME,USER_CREATE_DATE,USER_PROFILE_PIC,USER_TYPE,IS_ACTIVE)" +
								  " values(?,?,?,?,?,?,NOW(),?,?,?)";
						  ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
						  ps.setString(1, agentUsername);
						  ps.setString(2, agentDto.getAgentFirstName()+"@"+agentDto.getAgentLastName());
						  ps.setInt(3, agentId.intValue());
						  ps.setInt(4, agentDto.getCompanyId());
						  ps.setString(5, agentDto.getAgentFirstName());
						  ps.setString(6, agentDto.getAgentLastName());
						  ps.setString(7, "avatar.png");
						  ps.setString(8, "2");
						  ps.setString(9, "yes");
						  ps.execute();
						  break;
					  }
				  }
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
			return "{\"operation\":\"failed\"}";
		}
		return "{\"result\":\"success\",\"username\":\""+agentUsername+"\",\"password\":\""+agentDto.getAgentFirstName()+"@"+agentDto.getAgentLastName()+"\"}";
	}
	@Override
	public String modifyAgent(AgentDto agentDto) {
		ResultSet rs;
		String query = "UPDATE agent SET AGENT_FIRST_NAME = ?, AGENT_LAST_NAME = ?, AGENT_EMAIL = ?, AGENT_PHONE =? ,MESSAGE = ?, ADDRESS = ? WHERE AGENT_ID = ? AND USER_ID = ? AND COMPANY_ID = ?";
		try{
			PreparedStatement ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, agentDto.getAgentFirstName());
			ps.setString(2, agentDto.getAgentLastName());
			ps.setString(3, agentDto.getAgentEmail());
			ps.setString(4, agentDto.getAgentPhone());
			ps.setString(5, agentDto.getMessage());
			ps.setString(6, agentDto.getAddress());
			ps.setInt(7, agentDto.getAgentId());
			ps.setInt(8, agentDto.getUserId());
			ps.setInt(9, agentDto.getCompanyId());
			ps.execute();

			String query2 = "UPDATE user SET PASSWORD = ?, USER_FIRST_NAME = ?, USER_LAST_NAME=?, USER_PROFILE_PIC = ? WHERE AGENT_ID = ? AND COMPANY_ID = ? AND USERNAME = ?";
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query2, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, agentDto.getPassword());
			ps.setString(2, agentDto.getAgentFirstName());
			ps.setString(3, agentDto.getAgentLastName());
			ps.setString(4, agentDto.getProfilepic());
			ps.setInt(5, agentDto.getAgentId());
			ps.setInt(6, agentDto.getCompanyId());
			ps.setString(7, agentDto.getUserName());
			ps.execute();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
			return "{\"operation\":\"failed\"}";
		}
		return "{\"result\":\"success\",\"username\":\""+agentDto.getUserName()+"\",\"password\":\""+agentDto.getAgentFirstName()+"@"+agentDto.getAgentLastName()+"\"}";
	}
	@Override
	public List<AgentDto> getAgentByUserId(Integer userId,Integer companyId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AgentDto> agentList = new ArrayList();
		String query = "SELECT AGENT_ID,COMPANY_ID,USER_ID,AGENT_FIRST_NAME,AGENT_LAST_NAME,AGENT_EMAIL,AGENT_PHONE,MESSAGE,ADDRESS,CREATE_DATE FROM agent WHERE USER_ID = ? AND COMPANY_ID = ? ORDER BY CREATE_DATE DESC";
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, userId);
			ps.setInt(2, companyId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Integer agentId = rs.getInt("AGENT_ID");
				userId = rs.getInt("USER_ID");
				String agentFirstName = rs.getString("AGENT_FIRST_NAME");	
				String agentLastName = rs.getString("AGENT_LAST_NAME");	
				String agentEmail = rs.getString("AGENT_EMAIL");	
				String agentPhone = rs.getString("AGENT_PHONE");	
				String message = rs.getString("MESSAGE");	
				String address = rs.getString("ADDRESS");	
				String createDate = rs.getString("CREATE_DATE");
				companyId = rs.getInt("COMPANY_ID");
				agentList.add(new AgentDto(agentId,companyId,userId,agentFirstName,agentLastName,agentEmail,agentPhone,message,address,createDate));
				System.out.println("getAgentByUserId ");
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return agentList;
	}
	@Override
	public AgentDto getAgentProfileData(Integer userId, Integer companyId, Integer agentid){
		PreparedStatement ps = null;
		ResultSet rs = null;
		AgentDto agentprofileData = new AgentDto();
		String query = "SELECT A.AGENT_ID,A.COMPANY_ID,A.USER_ID,A.AGENT_FIRST_NAME,A.AGENT_LAST_NAME,A.AGENT_EMAIL,A.AGENT_PHONE,A.MESSAGE,A.ADDRESS,A.CREATE_DATE,U.PASSWORD,U.USER_PROFILE_PIC,U.USERNAME FROM agent A, user U WHERE A.AGENT_ID = U.AGENT_ID AND A.USER_ID = ? AND A.COMPANY_ID = ? and A.AGENT_ID = ? ORDER BY CREATE_DATE DESC";
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, userId);
			ps.setInt(2, companyId);
			ps.setInt(3, agentid);
			rs = ps.executeQuery();
			while (rs.next()) {
				Integer agentId = rs.getInt("AGENT_ID");
				userId = rs.getInt("USER_ID");
				String agentFirstName = rs.getString("AGENT_FIRST_NAME");	
				String agentLastName = rs.getString("AGENT_LAST_NAME");	
				String agentEmail = rs.getString("AGENT_EMAIL");	
				String agentPhone = rs.getString("AGENT_PHONE");	
				String message = rs.getString("MESSAGE");	
				String address = rs.getString("ADDRESS");	
				String createDate = rs.getString("CREATE_DATE");
				companyId = rs.getInt("COMPANY_ID");
				String password = rs.getString("PASSWORD");
				String profilePic = rs.getString("USER_PROFILE_PIC");
				String userName = rs.getString("USERNAME");
				agentprofileData = (new AgentDto(agentId,companyId,userId,agentFirstName,agentLastName,agentEmail,agentPhone,message,address,createDate,password,profilePic,userName));
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return agentprofileData;
	
	}
	
	@Override
	public List<DealerDto> getDealerByAdminId(Integer userId,Integer companyId,Integer agentId) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<DealerDto> dealerList = new ArrayList();
		String query = "SELECT D.AGENT_ID,D.DEALER_ID,D.DEALER_FIRST_NAME,D.DEALER_LAST_NAME,D.DEALER_EMAIL,D.DEALER_PHONE,D.MESSAGE,D.REGISTER_DATE,D.LATITUDE,D.LONGITUDE,A.AGENT_FIRST_NAME, A.AGENT_LAST_NAME ";
		query += " FROM dealer D LEFT JOIN agent A on A.AGENT_ID = D.AGENT_ID WHERE A.USER_ID = ? and A.COMPANY_ID = ? ";
		if(agentId!=-1){
			query += " AND A.AGENT_ID = ? ";
		}
		query += " ORDER BY REGISTER_DATE DESC";
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, userId);
			ps.setInt(2, companyId);
			if(agentId!=-1){
				ps.setInt(3, agentId);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				agentId = rs.getInt("AGENT_ID");
				Integer dealerId = rs.getInt("DEALER_ID");
				String firstName = rs.getString("DEALER_FIRST_NAME");	
				String lastName = rs.getString("DEALER_LAST_NAME");	
				String email = rs.getString("DEALER_EMAIL");	
				String phone = rs.getString("DEALER_PHONE");	
				String message = rs.getString("MESSAGE");	
				String registerDate = rs.getString("REGISTER_DATE");	
				String latitude = rs.getString("LATITUDE");	
				String longitude = rs.getString("LONGITUDE");	
				String agentFirstName =  rs.getString("AGENT_FIRST_NAME");
				String agentLastName =  rs.getString("AGENT_LAST_NAME");
				dealerList.add(new DealerDto(agentId, dealerId, firstName, lastName, email, phone, message,registerDate,latitude,longitude,agentFirstName,agentLastName));
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return dealerList;
	}
	@Override
	public DealerDto getDealerDetails(Integer userId, Integer companyId, Integer dealerId, String fromWhere) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int i = 1;
		DealerDto dealerDetails = new DealerDto();
		String query = "SELECT D.AGENT_ID,D.DEALER_ID,D.DEALER_FIRST_NAME,D.DEALER_LAST_NAME,D.DEALER_EMAIL,D.DEALER_PHONE,D.MESSAGE,D.REGISTER_DATE,D.LATITUDE,D.LONGITUDE,A.AGENT_FIRST_NAME, A.AGENT_LAST_NAME ";
		query += " FROM dealer D LEFT JOIN agent A on A.AGENT_ID = D.AGENT_ID WHERE ";
		if("admin".equals(fromWhere.trim())){
			query += " A.USER_ID = ? and";
		}
		query += " A.COMPANY_ID = ? and D.DEALER_ID = ?";
		query += " ORDER BY REGISTER_DATE DESC";
		try{
			ps = DatabaseConnection.getDatabaseConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			if("admin".equals(fromWhere.trim())){
				ps.setInt(i++, userId);
			}
			ps.setInt(i++, companyId);
			ps.setInt(i++, dealerId);

			rs = ps.executeQuery();
			while (rs.next()) {
				Integer agentId = rs.getInt("AGENT_ID");
				dealerId = rs.getInt("DEALER_ID");
				String firstName = rs.getString("DEALER_FIRST_NAME");	
				String lastName = rs.getString("DEALER_LAST_NAME");	
				String email = rs.getString("DEALER_EMAIL");	
				String phone = rs.getString("DEALER_PHONE");	
				String message = rs.getString("MESSAGE");	
				String registerDate = rs.getString("REGISTER_DATE");	
				String latitude = rs.getString("LATITUDE");	
				String longitude = rs.getString("LONGITUDE");	
				String agentFirstName =  rs.getString("AGENT_FIRST_NAME");
				String agentLastName =  rs.getString("AGENT_LAST_NAME");
				dealerDetails = new DealerDto(agentId, dealerId, firstName, lastName, email, phone, message,registerDate,latitude,longitude,agentFirstName,agentLastName);
			}
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return dealerDetails;
	}
}
