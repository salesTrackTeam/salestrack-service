package com.salestrack.app.service.impl;

import java.sql.PreparedStatement;
import java.sql.Statement;

import com.salestrack.app.connection.DatabaseConnection;
import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.EnquiryDto;
import com.salestrack.app.service.AgentService;
import com.salestrack.app.service.HomeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeServiceimpl implements HomeService{

	@Override
	public String SubmitEnquiry(EnquiryDto enquiryDto) {
		String query = "INSERT INTO enquiry(NAME,BUSINESS,PHONE,EMAIL,ENQUIRY_DATE,STATUS) values(?,?,?,?,NOW(),?)";
		try{
			PreparedStatement ps = DatabaseConnection.getDatabaseNewConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1,enquiryDto.getName());
			ps.setString(2, enquiryDto.getBusiness());
			ps.setString(3, enquiryDto.getPhone());
			ps.setString(4, enquiryDto.getEmail());
			ps.setString(5, "NEW");
			ps.execute();
			ps.close();
			return "{\"result\":\"success\"}";
		}catch(Exception e){
			System.out.print("Exception in SubmitEnquiry()");
			e.printStackTrace();
			return "{\"result\":\"failed\"}";
		}
	}
}
