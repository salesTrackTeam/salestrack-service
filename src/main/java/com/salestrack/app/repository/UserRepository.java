package com.salestrack.app.repository;

import com.salestrack.app.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Prashant on 13/5/17.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
}
