package com.salestrack.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.UserDto;
import com.salestrack.app.service.AgentService;
import com.salestrack.app.utils.Constants;

/**
 * Created by Prashant on 25/10/2018.
 */
@RequestMapping("/agent")
@RestController
public class AgentController {
	@Autowired
	AgentService agentService;

	@RequestMapping(Constants.GET_DEALER_BY_AGENT_ID)
	public List<DealerDto> getDealerByAgentId(@PathVariable Integer agentId) {
		return agentService.getDealerByAgentId(agentId);
	}
	
	@RequestMapping(value= Constants.SAVE_DEALER, method= RequestMethod.POST)
	public String saveDealer(@RequestBody DealerDto dealerDto) {
		System.out.println("In saveDealer() method--");
		return agentService.AddDealer(dealerDto);
	}
}
