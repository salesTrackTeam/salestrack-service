package com.salestrack.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.AgentDto;
import com.salestrack.app.dto.AdminDashboardDto;
import com.salestrack.app.service.AdminService;
import com.salestrack.app.utils.Constants;

/**
 * Created by Prashant on 25/10/2018.
 */
@RequestMapping("/admin")
@RestController
public class AdminController {
	@Autowired
	AdminService adminService;

	@RequestMapping(Constants.GET_ADMIN_DASHBOARD_DATA)
	public AdminDashboardDto getAdminDashboardData(@PathVariable Integer userId,@PathVariable Integer companyId) {
		return adminService.getAdminDashboardData(userId,companyId);
	}
	
	@RequestMapping(Constants.GET_AGENT_BY_ADMIN_ID)
	public List<AgentDto> getAgentByAgentId(@PathVariable Integer userId,@PathVariable Integer companyId) {
		return adminService.getAgentByUserId(userId,companyId);
	}
	@RequestMapping(Constants.GET_AGENT_PROFILEDATA)
	public AgentDto getAgentProfileData(@PathVariable Integer userId,@PathVariable Integer companyId,@PathVariable Integer agentId) {
		return adminService.getAgentProfileData(userId,companyId,agentId);
	}
	@RequestMapping(Constants.GET_DEALER_DETAILS)
	public DealerDto getDealerDetails(@PathVariable Integer userId,@PathVariable Integer companyId,@PathVariable Integer dealerId,@PathVariable String fromWhere) {
		return adminService.getDealerDetails(userId,companyId,dealerId,fromWhere);
	}
	@RequestMapping(value= Constants.ADD_AGENT, method= RequestMethod.POST)
	public String addAgent(@RequestBody AgentDto agentDto) {
		System.out.println("In saveAgent() method--"+agentDto+agentDto.getAgentEmail());
		return adminService.addAgent(agentDto);
	}
	@RequestMapping(value= Constants.MODIFY_AGENT, method= RequestMethod.POST)
	public String modifyAgent(@RequestBody AgentDto agentDto) {
		System.out.println("In modifyAgent() method--"+agentDto+agentDto.getAgentEmail());
		return adminService.modifyAgent(agentDto);
	}
	@RequestMapping(value= Constants.GET_DEALER_BY_ADMIN_ID)
	public List<DealerDto> getDealerByAdminId(@PathVariable Integer userId,@PathVariable Integer companyId,@PathVariable Integer agentId) {
		System.out.println("In saveAgent() method-- getDealerByAdminId");
		return adminService.getDealerByAdminId(userId, companyId, agentId);
	}
}
