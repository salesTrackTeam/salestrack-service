package com.salestrack.app.controller;

import java.util.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.salestrack.app.dto.DealerDto;
import com.salestrack.app.dto.EnquiryDto;
import com.salestrack.app.service.HomeService;
import com.salestrack.app.utils.Constants;

@RestController
public class HomeController {
	@Autowired
	HomeService homeService;
	
	@RequestMapping(value= Constants.ENQUIRY, method= RequestMethod.POST)
	public String addEnquiry(@RequestBody EnquiryDto enquiryDto) {
		System.out.println("In addEnquiry() method--");
		return homeService.SubmitEnquiry(enquiryDto);
	}
}
