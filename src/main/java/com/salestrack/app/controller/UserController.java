package com.salestrack.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.salestrack.app.dto.UserDto;
import com.salestrack.app.service.UserService;
import com.salestrack.app.utils.Constants;

/**
 * Created by Prashant on 25/10/2018.
 */
@RequestMapping("/user")
@RestController
public class UserController {
	@Autowired
	UserService userService;

	@RequestMapping(Constants.AUTHENTICATE)
	public UserDto getUserById(@PathVariable String company,@PathVariable String username,@PathVariable String password) {
		System.out.println("111"+company+username+password);
		return userService.authenticate(company,username,password);
	}
	/*
	 * @author: Mukesh Singla
	 * @Purpose: This method used to add customer in the system during Sign Up process.
	 */
	@RequestMapping(value= Constants.SIGNUP, method= RequestMethod.POST)
	public String signup(@RequestBody UserDto userDto) {
		System.out.println("In Signup method--");
		return userService.signup(userDto);
	}
}
