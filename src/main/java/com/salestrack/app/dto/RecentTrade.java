package com.salestrack.app.dto;

public class RecentTrade{
	String agentName;
	String date;
	String amount;
	String dealerName;
	
	public RecentTrade(String agentName,String date,String amount){
		this.agentName = agentName;
		this.date = date;
		this.amount = amount;
	}
	public RecentTrade(String agentName,String date,String amount,String dealerName){
		this.agentName = agentName;
		this.date = date;
		this.amount = amount;
		this.dealerName = dealerName;
	}
	
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	
}
