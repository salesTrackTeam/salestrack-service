package com.salestrack.app.dto;

import java.util.ArrayList;
import java.util.List;
import com.salestrack.app.dto.RecentTrade;
/**
 * Created by Prashant on 25/10/2018.
 */
public class AdminDashboardDto {

	String monthlyIncome;
    String orders;
	String visits;
    String userActivity;
    List<RecentTrade> recentTrade;
    List<RecentAgentActivity> recentAgentActivity;
    List<ToDoList> toDoList;
	List<CompositeTransaction> compositeTransaction;

	public AdminDashboardDto() {

    }
    public AdminDashboardDto(
    		String monthlyIncome,
    		String orders,
    		String visits,
    		String userActivity,
    		List<RecentTrade> recentTrade,
    		List<RecentAgentActivity> recentAgentActivity,
    		List<ToDoList> toDoList,
    		List<CompositeTransaction> compositeTransaction) {
    	
    	this.monthlyIncome = monthlyIncome;
    	this.orders = orders;
    	this.visits = visits;
    	this.userActivity = userActivity;
    	this.recentTrade = recentTrade;
    	this.recentAgentActivity =recentAgentActivity;
    	this.toDoList = toDoList;
    	this.compositeTransaction =compositeTransaction;
    }
    
	public String getMonthlyIncome() {
		return monthlyIncome;
	}

	public void setMonthlyIncome(String monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}

	public String getOrders() {
		return orders;
	}

	public void setOrders(String orders) {
		this.orders = orders;
	}

	public String getVisits() {
		return visits;
	}

	public void setVisits(String visits) {
		this.visits = visits;
	}

	public String getUserActivity() {
		return userActivity;
	}

	public void setUserActivity(String userActivity) {
		this.userActivity = userActivity;
	}

	public List<RecentTrade> getRecentTrade() {
		return recentTrade;
	}

	public void setRecentTrade(List<RecentTrade> recentTrade) {
		this.recentTrade = recentTrade;
	}

	public List<RecentAgentActivity> getRecentAgentActivity() {
		return recentAgentActivity;
	}

	public void setRecentAgentActivity(List<RecentAgentActivity> recentAgentActivity) {
		this.recentAgentActivity = recentAgentActivity;
	}

	public List<ToDoList> getToDoList() {
		return toDoList;
	}

	public void setToDoList(List<ToDoList> toDoList) {
		this.toDoList = toDoList;
	}

	public List<CompositeTransaction> getCompositeTransaction() {
		return compositeTransaction;
	}

	public void setCompositeTransaction(
			List<CompositeTransaction> compositeTransaction) {
		this.compositeTransaction = compositeTransaction;
	}
  }