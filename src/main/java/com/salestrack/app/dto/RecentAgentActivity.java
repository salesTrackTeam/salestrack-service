package com.salestrack.app.dto;

public class RecentAgentActivity{
	String status;
	String date;
	String user;
	String value;
	String noOfDealerAdded;
	
	public RecentAgentActivity(	String status,String date,String user,String value){
		this.status = status;
		this.date = date;
		this.user = user;
		this.value = value;
	}
	public RecentAgentActivity(	String status,String date,String user,String value,String noOfDealerAdded){
		this.status = status;
		this.date = date;
		this.user = user;
		this.value = value;
		this.noOfDealerAdded = noOfDealerAdded;
	}
	
	public String getNoOfDealerAdded() {
		return noOfDealerAdded;
	}
	public void setNoOfDealerAdded(String noOfDealerAdded) {
		this.noOfDealerAdded = noOfDealerAdded;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
