package com.salestrack.app.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prashant on 25/10/2018.
 */
public class UserDto {

	String userId;
	String companyId;
	String companyLoginName;
	String companyName;
    String userFirstName;
    String userLastName;
    String userRole;
    String userType;
    String profilePic;
    String email;
    String phone;
    String address;
	String isActive;
	String deactivationDate;
    String loginFlag;
    String agentId;
    String registrationType;
    String password;
    String userName;
    String companyType;
    /*
     * loginFlag
     * 1:Login SuccessFull
     * 2:Incorrect UserId Or Password
     * 3:User is Inactive
     * 4:Company is Inactive
     * 5:User or company Does not exist
     * */
    public UserDto(){}
    public UserDto(	String userId,
    				String companyId,
    				String companyLoginName,
    				String companyName,
				    String userFirstName,
				    String userLastName,
				    String userRole,
				    String userType,
				    String profilePic,
				    String email,
				    String phone,
				    String address,
				    String isActive,
				    String deactivationDate,
				    String loginFlag,
				    String agentId) {
    	this.userId = userId;
    	this.companyId = companyId;
        this.companyLoginName = companyLoginName;
        this.companyName = companyName;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userRole = userRole;
        this.userType = userType;
        this.profilePic = profilePic;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.isActive = isActive;
        this.deactivationDate = deactivationDate;
        this.loginFlag = loginFlag;
        this.agentId = agentId;
    }
    public UserDto(	String userId,
			String companyId,
			String companyLoginName,
			String companyName,
		    String userFirstName,
		    String userLastName,
		    String userRole,
		    String userType,
		    String profilePic,
		    String email,
		    String phone,
		    String address,
		    String isActive,
		    String deactivationDate,
		    String loginFlag,
		    String agentId,
		    String registrationType,
		    String password,
		    String userName,
		    String companyType) {
this.userId = userId;
this.companyId = companyId;
this.companyLoginName = companyLoginName;
this.companyName = companyName;
this.userFirstName = userFirstName;
this.userLastName = userLastName;
this.userRole = userRole;
this.userType = userType;
this.profilePic = profilePic;
this.email = email;
this.phone = phone;
this.address = address;
this.isActive = isActive;
this.deactivationDate = deactivationDate;
this.loginFlag = loginFlag;
this.agentId = agentId;
this.registrationType = registrationType;
this.password = password;
this.userName = userName;
this.companyType = companyType;
}
    public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getCompanyLoginName() {
		return companyLoginName;
	}
	public void setCompanyLoginName(String companyLoginName) {
		this.companyLoginName = companyLoginName;
	}
	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
    public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getDeactivationDate() {
		return deactivationDate;
	}
	public void setDeactivationDate(String deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	public String getLoginFlag() {
		return loginFlag;
	}
	public void setLoginFlag(String loginFlag) {
		this.loginFlag = loginFlag;
	}

	public String getRegistrationType() {
		return registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
}
