package com.salestrack.app.dto;

public class CompositeTransaction{
	String transaction;
	String date;
	String amount;
	public CompositeTransaction(String transaction,String date,String amount){
		this.transaction = transaction;
		this.date = date;
		this.amount = amount;
	}
	public String getTransaction() {
		return transaction;
	}
	public void setTransaction(String transaction) {
		this.transaction = transaction;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

}
