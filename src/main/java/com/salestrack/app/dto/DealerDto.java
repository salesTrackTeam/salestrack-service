package com.salestrack.app.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prashant on 25/10/2018.
 */
public class DealerDto {
    Integer agentId;
    Integer dealerId;
	String firstName;
    String lastName;
    String email;
    String phone;
    String message;
    String registerDate;
    String latitude;
    String longitude;
    String agentFirstName;
    String agentLastName;


    public DealerDto(Integer agentId,Integer dealerId,String firstName,String lastName,String email,String phone,String message,String registerDate,String latitude, String longitude) {
        this.agentId =  agentId;
        this.dealerId = dealerId;
        this.firstName =  firstName;
        this.lastName =  lastName;
        this.email =  email;
        this.phone = phone;
        this.message =  message;
        this.registerDate = registerDate;
        this.latitude = latitude;
        this.longitude = longitude;
    }
	public DealerDto(Integer agentId,Integer dealerId,String firstName,String lastName,String email,String phone,String message,String registerDate,String latitude, String longitude, String agentFirstName, String agentLastName) {
        this.agentId =  agentId;
        this.dealerId = dealerId;
        this.firstName =  firstName;
        this.lastName =  lastName;
        this.email =  email;
        this.phone = phone;
        this.message =  message;
        this.registerDate = registerDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.agentFirstName = agentFirstName;
        this.agentLastName = agentLastName;
    }

    public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public DealerDto() {
    }

    public String getRegisterDate() {
    	return registerDate;
    }
    public void setRegisterDate(String registerDate) {
    	this.registerDate = registerDate;
    }
    public Integer getDealerId() {
    	return dealerId;
    }
    public void setDealerId(Integer dealerId) {
    	this.dealerId = dealerId;
    }
    
    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
    
}
