package com.salestrack.app.dto;

public class ToDoList{
	String toDo;
	public ToDoList(String toDo){
		this.toDo = toDo;
	}
	public String getToDo() {
		return toDo;
	}

	public void setToDo(String toDo) {
		this.toDo = toDo;
	}
}