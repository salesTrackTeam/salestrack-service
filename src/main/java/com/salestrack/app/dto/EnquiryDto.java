package com.salestrack.app.dto;

/**
 * Created by Prashant on 13/5/17.
 */
public class EnquiryDto {

	String name;
	String business;
	String phone;
	String email;
	public EnquiryDto(){
	}
	public EnquiryDto(String name, String business, String phone, String email) {
		this.name = name;
		this.business = business;
		this.phone = phone;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBusiness() {
		return business;
	}
	public void setbusiness(String business) {
		this.business = business;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

}
