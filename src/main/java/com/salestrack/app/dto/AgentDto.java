package com.salestrack.app.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Prashant on 25/10/2018.
 */
public class AgentDto {

	Integer agentId;
	Integer companyId;
	Integer userId;
	String agentFirstName;
    String agentLastName;
    String agentEmail;
    String agentPhone;
    String message;
    String address;
	String createDate;
	String password;
	String profilepic;
	String userName;

	public AgentDto(Integer agentId,Integer companyId,Integer userId,String agentFirstName,String agentLastName,String agentEmail,String agentPhone,String message,String address,String createDate,String password,String profilepic,String userName ) {
        this.agentId =  agentId;
        this.userId = userId;
        this.agentFirstName =  agentFirstName;
        this.agentLastName =  agentLastName;
        this.agentEmail =  agentEmail;
        this.agentPhone = agentPhone;
        this.message =  message;
        this.address = address;
        this.createDate = createDate;
        this.companyId = companyId;
        this.password = password;
        this.profilepic = profilepic;
        this.userName = userName;
    }

	public AgentDto(Integer agentId,Integer companyId,Integer userId,String agentFirstName,String agentLastName,String agentEmail,String agentPhone,String message,String address,String createDate) {
        this.agentId =  agentId;
        this.userId = userId;
        this.agentFirstName =  agentFirstName;
        this.agentLastName =  agentLastName;
        this.agentEmail =  agentEmail;
        this.agentPhone = agentPhone;
        this.message =  message;
        this.address = address;
        this.createDate = createDate;
        this.companyId = companyId;
    }

    public AgentDto() {
    }
    public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
    public Integer getAgentId() {
		return agentId;
	}

	public void setAgentId(Integer agentId) {
		this.agentId = agentId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAgentFirstName() {
		return agentFirstName;
	}

	public void setAgentFirstName(String agentFirstName) {
		this.agentFirstName = agentFirstName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public String getAgentEmail() {
		return agentEmail;
	}

	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	public String getAgentPhone() {
		return agentPhone;
	}

	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfilepic() {
		return profilepic;
	}

	public void setProfilepic(String profilepic) {
		this.profilepic = profilepic;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
