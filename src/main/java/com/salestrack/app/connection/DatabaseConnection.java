package com.salestrack.app.connection;

import java.sql.*;
import java.sql.DriverManager;
/**
 * @desc A singleton database access class for MySQL
 * @author Prashant Verma
 */
public final class DatabaseConnection {
	
    public static Connection connection;
    private DatabaseConnection() {
        //String url= "jdbc:mysql://localhost:3306/";
    	//String userName = "root";
        //String password = "system";
        //String dbName = "salestrack";
		
        //String url= "jdbc:mysql://aa1qrd5l9o1t76l.ckorvjbkwfai.ap-south-1.rds.amazonaws.com:3306/";//Production
        //String dbName = "salespower";//For Production

    	//String url= "jdbc:mysql://salestrack.ckorvjbkwfai.ap-south-1.rds.amazonaws.com:3306/";//For Testing
    	//String dbName = "salestrack";//For Testing

    	//String userName = "root";
        //String password = "system123";
        
        
        String url= "jdbc:mysql://ec2-13-232-238-73.ap-south-1.compute.amazonaws.com:3306/";
    	String userName = "root";
        String password = "System@123";
        String dbName = "salespower";
        
        String driver = "com.mysql.jdbc.Driver";
        
        try {
            Class.forName(driver).newInstance();
            connection = (Connection)DriverManager.getConnection(url+dbName,userName,password);
        }
        catch (Exception sqle) {
            sqle.printStackTrace();
        }
    }
    /**
     *
     * @return MysqlConnect Database connection object
     */
    public static synchronized Connection getDatabaseConnection() {
        if ( connection == null ) {
        	new DatabaseConnection();
        }
        return connection;
    }
    public static synchronized Connection getDatabaseNewConnection() {
        new DatabaseConnection();
        return connection;
    }
 
}