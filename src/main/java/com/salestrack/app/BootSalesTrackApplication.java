package com.salestrack.app;

import com.salestrack.app.entity.Skill;
import com.salestrack.app.entity.User;
import com.salestrack.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
public class BootSalesTrackApplication {
	@Autowired
	UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(BootSalesTrackApplication.class, args);
	}

	@PostConstruct
	public void setupDbWithData(){
		User user= new User("Prashant", null);
		user.setSkills(Arrays.asList(new Skill("java"), new Skill("js")));
		user= userRepository.save(user);
	}
}
